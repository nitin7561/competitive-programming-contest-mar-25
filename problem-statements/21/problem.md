**Problem Statement 21**


You are given an N by N square grid that represents a pond that is filled with honey. A rock of mass M kg is dropped in the pond on one of the squares in the grid. Instantaneously, the height of the pond in the square in which it is dropped rises to M cm high. All squares bordering this particular square rises to M - 1 cm, and so on “radially” outward decreasing by a unit each time. 


For the sake of example, suppose the pond is represented by a grid of 9 by 9 squares. 


Let the squares be numbered in a matrix form. That is, S<sub>ij</sub> represents the square in the i<sup>th</sup> row and j<sup>th</sup> column. i and j range from 0 to N - 1. In the case of the example, i and j range from 0 to 8.


Let the rock be dropped on the square S<sub>4,4</sub>. The heights of all the squares in the grid are as in the figure below:


0 0 0 0 0 0 0 0 0

0 0 0 0 0 0 0 0 0

0 0 1 1 1 1 1 0 0

0 0 1 2 2 2 1 0 0

0 0 1 2 3 2 1 0 0

0 0 1 2 2 2 1 0 0

0 0 1 1 1 1 1 0 0

0 0 0 0 0 0 0 0 0

0 0 0 0 0 0 0 0 0


Suppose another rock of mass 2 is dropped at position S<sub>7,6</sub>. Then, as usual, the heights of squares in bordering region decrease analogously to the previous case. But if the height of the pond in a square is not zero, then the heights are added.


(The same state is achieved in all cases whether the two rocks are dropped simultaneously or after the other.) Here’s the situation after both these rocks are dropped (positions of the rocks are marked in bold; note that the square S<sub>6,6</sub> is of height 2 because its height’s affected by both the rocks):



0 0 0 0 0 0 0 0 0

0 0 0 0 0 0 0 0 0

0 0 1 1 1 1 1 0 0

0 0 1 2 2 2 1 0 0

0 0 1 2 3 2 1 0 0 

0 0 1 2 2 2 1 0 0

0 0 1 1 1 1 2 1 1

0 0 0 0 0 0 1 2 1

0 0 0 0 0 0 1 1 1


The maximum height of the pond is 3 cm.


Your task is simple:


You are given a pond filled with honey represented by a square grid of N by N squares. Furthermore, the height of the pond in the squares in the grid is 0. R rocks of mass M<sub>i</sub> where i ranges from 0 to R are dropped in different positions on the grid. Find the maximum height of the pond.


**Input format**

The first line of input consists of the integer T. This is the number of test cases. The first line of each test case consists of two integers N and R. Then R lines follow each consisting of three integers M i j. This means that a mass of M is dropped on the square S<sub>ij</sub>.


**Output format**

For each testcase, output an integer X which is the maximum height of the pond (as described above.)


**Sample Input**
```
1
8 3
3 4 4
2 6 6
4 5 4
```

**Sample Output**
```
6
```

